
        <FORM NAME="GetName" ACTION="dashboard.cfm" METHOD="post"> 
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="row block" id="reg-area">
            <div class="row block">
                <h2 id="form-header">Register</h2>
            </div>
            <div class="row">
                <cfif IsDefined ("URL.Message")>
                    <cfoutput><p id="error-text">The Passwords you entered did not match.</p></cfoutput>
                </cfif>
            </div>
            <div class="row block">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" class="form-control" placeholder="Please input valid email." required autofocus>
            </div>
            <div class="row block">
                <label for="username">Username</label>
                <input type="text" id="username" name="username" class="form-control" placeholder="Please input your username." required>
            </div>
            <div class="row block" hidden>
                Gender:
                <input type="radio"  class="form-control" name="Gender" value="Male">Male
                <input type="radio"  class="form-control" name="Gender" value="Female">Female
            </div>
            <div class="row block">
                <label for="password1">Password</label>
                <input id="password1" class="form-control" minlength="6" type="password" required name="password" placeholder="Minimal 6 characters.">
                <label for="password2">Reenter Password</label>
                <input id="password2" class="form-control" minlength="6" type="password" required name="repassword"  placeholder="Minimal 6 characters.">
            </div>
            <div class="row block">
                <button id="reg-button" class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </div>
            </form>
        </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
            </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
