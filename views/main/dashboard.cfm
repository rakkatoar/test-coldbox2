<cfif #password# eq #repassword#>
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="row block" id="reg-area">
            <div class="row block">
                <h2 id="form-header">My Data</h2>
            </div>
            <div class="row block">
                <label for="email">Email</label>
                <cfoutput><input type="email" id="email" name="email" class="form-control" placeholder="Email" required autofocus value="#email#" disabled></cfoutput>
            </div>
            <div class="row block">
                <label for="username">Username</label>
                <cfoutput><input type="text" id="username" name="username" class="form-control" placeholder="Username" required value="#username#" disabled></cfoutput>
            </div>
            <div class="row block">
                <label for="password1">Password</label>
                <cfoutput><input id="password1" class="form-control"  type=password required name=up placeholder="Password" value="#password#" disabled></cfoutput>
                <label for="password2">Reenter Password</label>
                <cfoutput><input id="password2" class="form-control"  type=password name=up2  placeholder="Password" value="#repassword#" disabled></cfoutput>
            </div>
            <div class="row" id="logout">
                
                    <h3 id="logout-text"><a href="index.cfm">Log Out</a></h3>
                
            </div>
        </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
            </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
<cfelse>
    <cflocation url = "../index.cfm?Message=1" addToken = "no">
</cfif>
        
